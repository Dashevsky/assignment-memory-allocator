#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "util.h"



static void line() { printf("\n------------------------------------------\n------------------------------------------\n"); }

static struct block_header* block_get_header(void* contents) {
	return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

// получить адрес после конца последнего блока
static void* after_last_block (struct block_header * block)
{
	while ((*block).next) block = (*block).next;
	return (*block).contents + (*block).capacity.bytes;
	
}


int main() {
	// создадим кучу
	void *heap;
	heap = heap_init(5000);
	
	line();
	printf("\nИНИЦИАЛИЗАЦИЯ КУЧИ\n");    
	debug_heap(stdout, heap);
	
	// ТЕСТ 1 ----------------------------------------------
	line();
	printf("\nОБЫЧНОЕ ВЫДЕЛЕНИЕ ПАМЯТИ\n\n");
	
	// выделим четыре блока и получим их заголовки
	// размер четвертого сделаем 1 (он должен будет увеличится до минимального - 24)
	void* block1 =  _malloc(101);
	struct block_header* block1h = block_get_header(block1);
	void* block2 =  _malloc(102);
	struct block_header* block2h = block_get_header(block2);
	void* block3 =  _malloc(103);
	struct block_header* block3h = block_get_header(block3);
	void* block4 =  _malloc(1);
	struct block_header* block4h = block_get_header(block4);
	
	debug_heap(stdout, heap);
	
	// проверим что все блоки нужного размера и помечены как занятые
	if (block1 && block2 && block3 && block4 && !(*block1h).is_free && !(*block2h).is_free && !(*block3h).is_free && !(*block4h).is_free &&
		(*block1h).capacity.bytes == 101 && (*block2h).capacity.bytes == 102 && (*block3h).capacity.bytes == 103 && (*block4h).capacity.bytes == 24)
		printf("\nВсе блоки выделены верно\n\nТЕСТ ПРОЙДЕН\n");
	else
		err("\nОшибка при выделении памяти\n\nТЕСТ ПРОВАЛЕН\n");
	
	// ТЕСТ 2 ----------------------------------------------
	line();
	printf("\nОСВОБОЖДЕНИЕ ОДНОГО БЛОКА\n");
	printf("ИЗ НЕСКОЛЬКИХ ВЫДЕЛЕННЫХ\n\n");
	
	debug_heap(stdout, heap);
	
	
	printf("\nОсвобождаем блок 3\n\n");
	_free(block3);
	debug_heap(stdout, heap);
	
	// проверим что все блоки нужной вместимости и помечены как занятые, кроме третьего
	if (block1 && block2 && block3 && block4 && !(*block1h).is_free && !(*block2h).is_free && (*block3h).is_free && !(*block4h).is_free &&
		(*block1h).capacity.bytes == 101 && (*block2h).capacity.bytes == 102 && (*block3h).capacity.bytes == 103 && (*block4h).capacity.bytes == 24)
		printf("\nБлок освобожден\n\nТЕСТ ПРОЙДЕН\n");
	else
		err("\nОшибка при освобождении блока\n\nТЕСТ ПРОВАЛЕН\n");
	
	// ТЕСТ 3 ----------------------------------------------
	line();
	printf("\nОСВОБОЖДЕНИЕ ДВУХ БЛОКОВ\n");
	printf("ИЗ НЕСКОЛЬКИХ ВЫДЕЛЕННЫХ\n\n");
	
	debug_heap(stdout, heap);
	
	printf("\nБлок 3 уже освобожден, освободим блок 2\n\n");
	_free(block2);
	debug_heap(stdout, heap);
	
	// проверим что все блоки нужной вместимости и помечены как занятые, кроме второго и третьего, которые объединятся
	// вместимость объединенного блока должена быть равена сумме объединенных + размер заголовка
	if (block1 && block2 && block4 && !(*block1h).is_free && (*block2h).is_free && !(*block4h).is_free &&
		(*block1h).capacity.bytes == 101 && (*block2h).capacity.bytes == 103+102+17 && (*block4h).capacity.bytes == 24)
		printf("\nБлоки освобождены\n\nТЕСТ ПРОЙДЕН\n");
	else
		err("\nОшибка при освобождении блоков\n\nТЕСТ ПРОВАЛЕН\n");
	
	// ТЕСТ 4 ----------------------------------------------
	line();
	printf("\nПАМЯТЬ ЗАКОНЧИЛАСЬ, НОВЫЙ РЕГИОН\n");
	printf("ПАМЯТИ РАСШИРЯЕТ СТАРЫЙ\n\n");
	
	debug_heap(stdout, heap);
	
	printf("\nПопробуем выделить блок, который ");
	printf("\nне поместится в куче\n\n");
	
	void* block5 =  _malloc(20000);
	struct block_header* block5h = block_get_header(block5);
	
	debug_heap(stdout, heap);
	
	if (block1 && block2 && block4 && block5 && !(*block1h).is_free && (*block2h).is_free && !(*block4h).is_free && !(*block5h).is_free &&
		(*block1h).capacity.bytes == 101 && (*block2h).capacity.bytes == 103+102+17 && (*block4h).capacity.bytes == 24 && (*block5h).capacity.bytes == 20000)
		printf("\nБольшой блок выделен успешно\n\nТЕСТ ПРОЙДЕН\n");
	else
		err("\nОшибка выделения блока\n\nТЕСТ ПРОВАЛЕН\n");
	
	// ТЕСТ 5 ----------------------------------------------
	line();
	printf("\nПАМЯТЬ ЗАКОНЧИЛАСЬ, СТАРЫЙ РЕГИОН НЕ РАСШИРИТЬ\n");
	printf("ИЗ-ЗА ДРУГОГО ВЫДЕЛЕННОГО ДИАПАЗОНА АДРЕСОВ,\n");
	printf("НОВЫЙ РЕГИОН ВЫДЕЛЯЕТСЯ В ДРУГОМ МЕСТЕ\n\n");
	
	// аллоцируем часть памяти, начиная с адреса после конца последнего блока кучи, чтобы блокировать ее расширение вплотную
	mmap(after_last_block((struct block_header *) heap), 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 0, 0);

	debug_heap(stdout, heap);
	printf("\nЗа последним свободным блоком кучи\n");
	printf("находится занятый регион размера 1000\n\n");
	
	printf("\nПопробуем выделить блок, который\n");
	printf("не поместится в последнем свободном\n\n");
	
	void* block6 = _malloc(20000);
	struct block_header* block6h = block_get_header(block6);
	debug_heap(stdout, heap);
	
	if (block6 && !(*block6h).is_free && (*block6h).capacity.bytes == 20000)
		printf("\nБлок успешно выделен в новом регионе в другом месте\n\nТЕСТ ПРОЙДЕН\n");
	else
		err("\nОшибка выделения блока\n\nТЕСТ ПРОВАЛЕН\n");
	line();
	
	printf("\nВСЕ ТЕСТЫ ПРОЙДЕНЫ\n\n");
	return 0;
}

