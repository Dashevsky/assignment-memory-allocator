#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );


static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
	.next = next,
	.capacity = capacity_from_size(block_sz),
	.is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}



/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query )
{
	// получим реальный размер необходимого блока памяти
	size_t required_region_size = region_actual_size(query);
	
	// попросим у nmap блок указанного размера по нужному нам адресу
	void* allocated_region_addr = map_pages(addr, required_region_size, MAP_FIXED_NOREPLACE);
	
	// флажок, который показывает, там ли, где мы хотели, nmap нам выделил память
	bool extends = 1;
	
	// если nmap не дал нам память где мы хотим, то запросим ее по его усмотрению
	// и поменяем значение флажка
	if (allocated_region_addr == MAP_FAILED)
	{
		allocated_region_addr = map_pages(addr, required_region_size, 0);
		extends = 0;
	}
	// инициализируем блок размера required_region_size в новой памяти
	block_init(allocated_region_addr, (block_size) {required_region_size}, NULL);
	
	// вернем наш участок памяти в виде региона
	return (struct region) {.addr = allocated_region_addr, .size = required_region_size, .extends = extends};
}



static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}


static bool split_if_too_big( struct block_header* first_block, size_t query ) 
{
	bool is_splitted_if_too_big = 0;
	// размер второго блока = вместимость исходного блока - query
	// вместимость второго блока = capacity_from_size(вместимость исходного блока - query)
	// за вторым блоком следует тот, который следовал за первым
	// второй блок свободен (так и по умолчанию при инициализации)
	
	// размер первого блока = size_from_capacity(query)
	// вместимость первого блока = query
	// за первым блоком следует второй
	// первый блок занят (или нет? по идее все же свободен, мы ведь просто делим их)
	
	// если блок делИм
	if (block_splittable(first_block, query))
	{
		// создадим указатель на второй блок и инициализируем сам этот блок
		struct block_header* second_block = (struct block_header*)((uint8_t*)first_block + size_from_capacity((block_capacity){query}).bytes);
		block_init(second_block, (block_size){(*first_block).capacity.bytes - query}, (*first_block).next);
		
		// обновим данные по первому блоку, от которого мы отпилили кусок в пользу второго
		(*first_block).next = second_block;
		(*first_block).capacity.bytes = query;
		
		is_splitted_if_too_big = 1;
	}	
	return is_splitted_if_too_big;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
							   struct block_header const* fst,
							   struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}


static bool try_merge_with_next( struct block_header* block )
{
	bool is_merged_with_next = 0;
	// если следующий блок существует и объединим с этим	
	if ((*block).next != NULL && mergeable(block, (*block).next))
	{
		// к вместимости первого добавим размер второго
		// за первым теперь следует блок, который до этого следовал за вторым
		(*block).capacity.bytes += size_from_capacity((block_capacity){(*((*block).next)).capacity.bytes}).bytes;
		(*block).next = (*((*block).next)).next;
		is_merged_with_next = 1;
	}
	return is_merged_with_next;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t query )
{
	// если нам передали пустой указатель на блок, с которого начинать искать
	// то вернем ошибку
	if (block == NULL) return (struct block_search_result) {.type = BSR_CORRUPTED };
	
	// ищем подходящий блок, перебирая их по очереди, кроме последнего
	// (то есть того, у которого нет следующего)
	while ((*block).next != NULL)
	{
		// объединяем этот блок со всеми пустыми последующими по очереди, пока можем
		while (try_merge_with_next(block));
		
		// если нашли свободный блок нужного размера, то вернем его
		// в try_memalloc_existing мы потом попробует его разделить
		if ((*block).is_free && (*block).capacity.bytes >= query)
			return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
		
		// перейдем к следующему блоку
		block = (*block).next;
	}
	
	// к этому моменту block - это последний блок
	if ((*block).is_free && (*block).capacity.bytes >= query)
		// если он нам подходит, то вернем его
		return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
	else
		// если нет, то вернем то, что мы дошли до конца
		// и сам блок мы тоже вернем, чтобы куча потом знала к чему присоединять блок в новой памяти
		return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block };	
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block )
{
	// ишем в куче пустой блок размера query и записываем результат
	// обрабатываем результат
	struct block_search_result bsr = find_good_or_last(block, query);
	switch (bsr.type)
	{
		// в этом случае memalloc, из которого мы это вызываем 
		// тоже увидит BSR_CORRUPTED и вернет NULL
		case BSR_CORRUPTED: { return bsr; }
		// аналогично, memalloc выделит память и попробует еще раз
		case BSR_REACHED_END_NOT_FOUND: { return bsr; }
		
		// если нужный блок все же выделен
		// то попробуем отделить от него лишний кусок и вернем тот, что просили
		case BSR_FOUND_GOOD_BLOCK: 
		{
			split_if_too_big(bsr.block, query);
			return bsr;
		}
		
	}
	return bsr;
}


static struct block_header* grow_heap( struct block_header* restrict last, size_t query )
{
	// попробуем аллоцировать новый регион для кучи после последнего блока
	// если не получится, то alloc_region попробует найти его хоть где-нибудь
	struct region new_region = alloc_region(block_after(last), query);
	
	// изменяем последний блок и возвращаем
	(*last).next = new_region.addr;
	return (*last).next;

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start)
{
	// увеличим вместимость блока до минимальной, если она меньше
	if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
	
	// просим выделить блок в доступной куче и записываем результат
	// обрабатываем результат
	struct block_search_result bsr = try_memalloc_existing(query, heap_start);
	switch (bsr.type)
	{
		case BSR_CORRUPTED: { return NULL; }
		
		// вернем блок, если нашли хороший (не забываем пометить, что теперь он занят)
		case BSR_FOUND_GOOD_BLOCK: { (*(bsr.block)).is_free = false; return bsr.block; }
		
		// если не нашли, дойдя до конца
		case BSR_REACHED_END_NOT_FOUND: 
		{
			// выделяем еще один регион в куче (передавая туда последний блок)
			grow_heap(bsr.block, query);
			
			// просим блок еще раз, но уже с места последнего
			// чтобы не перебирать всю кучу с начала
			return memalloc(query, bsr.block);
		}
	}
	return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  /*  ??? */
  // пока можем объединять блоки (первый со следующими по очереди) - объединияем
  while (try_merge_with_next(header)) {};
}

